use core::time;
use std::{collections::HashMap, vec, thread::sleep, process::exit};

use player::Player;
use point::Point;
use utils::prompt;

pub mod action;
pub mod matrix;
pub mod player;
pub mod point;
pub mod utils;

struct Game {
    players: Vec<Player>,
    max_score: u64,
}

impl Default for Game {
    fn default() -> Self {
        Game {
            players: vec![],
            max_score: 1000
        }
    }
}

impl Game {
    fn start(&mut self) -> () {
        let num_players = prompt::<usize>("How many players: ");
        for i in 0..num_players {
            let name = format!("Abdul-{}", format_args!("{}", i));
            let p = Player {
                name,
                score: 0,
                strength: 0,
                starting_point: Point(0, 0),
                is_immobile: false,
            };
            self.players.push(p);
        }

        loop {
            for py in &mut self.players {
                if py.score == self.max_score {
                    println!("Congratulations {}, you won!!", py.name);
                    exit(0);
                }

                let act = action::generator::generate_action();
                match act {
                    action::Action::Displace(Point(x, y)) => {
                        if py.is_immobile {
                            continue;
                        }
                        let Point(_x, _y) = &mut py.starting_point;
                        *_x = *_x + x;
                        *_y = *_y + y;
                        println!(
                            "{} moved: Point({},{}) --> Point({},{}): score {}",
                            py.name, _x, _y, x, y, py.score
                        );
                    }
                    action::Action::Immobalize(state) => {
                        if state {
                            println!(
                                "{} immobalized: Point({},{})",
                                py.name, py.starting_point.0, py.starting_point.1
                            )
                        } else {
                            println!("{} is mobile", py.name);
                        }
                        py.is_immobile = state;
                    }
                    action::Action::Rotate(mat) => {
                        if py.is_immobile {
                            println!("{} Cannot rotate: its immobalized", py.name);
                            continue;
                        } else {
                            let Point(x, y) = py.starting_point;
                            py.starting_point = mat.transform(&py.starting_point);
                            println!(
                                "{} rotated: {} --> {}: score {}",
                                py.name,
                                Point(x, y),
                                py.starting_point,
                                py.score
                            );
                        }
                    }
                }
            }

            sleep(time::Duration::from_secs(3));

            let mut points: HashMap<u64, &mut Player> = HashMap::new();
            let mut max = 0u64;
            for p in &mut self.players {
                let distance = p.starting_point.mod_sq();
                if distance > max {
                    max = distance;
                }
                points.insert(distance, p);
            }

            if let Some(p) = points.get_mut(&max) {
                (*p).score += 1;
            }
        }
    }
}

fn main() {
    let mut game = Game{..Default::default()};
    game.start();
}
