use std::fmt::Display;

use crate::point::Point;


pub struct Player {
    pub name: String,
    pub score: u64,
    pub strength: u8,
    pub starting_point: Point,
    pub is_immobile: bool,
}

impl Player {
    pub fn set_name(&mut self, new_name: String) {
        self.name = new_name;
    }

    pub fn set_score(&mut self, score: u64) {
        self.score = score;
    }

    pub fn set_strength(&mut self, strength: u8) {
        self.strength = strength;
    }
}

impl Display for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Player(name: {})",self.name))
    }
}
