use std::fmt::Display;


pub struct Point(pub u64, pub u64);

impl Point {
    pub fn mod_sq(&self) -> u64 {
        (self.0*self.0) + (self.1*self.1)
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Point({},{})",self.0, self.1))
    }
}

pub mod generator {
    use rand::Rng;

    use super::Point;

    pub fn generate_point() -> Point {
        let mut rng = rand::thread_rng();
        let x: u64 = rng.gen();
        let y: u64 = rng.gen();
        Point(x%5, y%5)
    }
}

