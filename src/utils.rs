use core::panic;
use std::{io::{stdout, Write, stdin}, str::FromStr};



pub fn prompt<T: FromStr>(msg: &str) -> T {
    let mut result = String::new();
    print!("{}", msg);
    stdout().flush().unwrap();
    match stdin().read_line(&mut result) {
        Ok(_) => {},
        Err(_) => {
            panic!("Cannot read input");
        }
    }
    result = result.replace("\n", "");
    let res: T = match result.parse::<T>() {
        Ok(n) => n,
        Err(_) => {
            panic!("Cannot parse to integer")
        }
    };
    res
}
