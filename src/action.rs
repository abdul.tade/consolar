use crate::{matrix::Matrix, point::Point};

pub enum Action {
    Displace(Point),
    Immobalize(bool),
    Rotate(Matrix),
}

pub mod generator {
    use crate::{
        action::Action,
        point, matrix,
    };
    use rand::Rng;

    pub fn generate_action() -> Action {
        const MAX_ACTIONS: u8 = 3;
        let mut rng = rand::thread_rng();
        let choice: u8 = rng.gen();
        let choice: u8 = choice % MAX_ACTIONS;

        if choice == 0 {
            let p = point::generator::generate_point();
            return Action::Displace(p);
        }
        if choice == 1 {
            let t: u8 = rng.gen();
            return match t <= 127 {
                true => Action::Immobalize(true),
                false => Action::Immobalize(false),
            }
        }

        let m = matrix::generator::generate_matrix([2,2]);
        Action::Rotate(m)
    }
}
