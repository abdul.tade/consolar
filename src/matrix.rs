use crate::point::Point;

pub struct Matrix {
    mat: Vec<Vec<u64>>,
    shape: [usize; 2],
}

impl Matrix {
    pub fn transform(&self, p: &Point) -> Point {
        let Point(x, y) = p;
        if !(self.shape[0] == 2 && self.shape[1] == 2) {
            panic!("Matrix has to be 2x2");
        }

        let r1 = &self.mat[0];
        let r2 = &self.mat[1];

        let _x = (r1[0] * x) + (r1[0] * y);
        let _y = (r2[0] * x) + (r2[0] * y);

        Point(_x, _y)
    }

    pub fn new(shape: [usize; 2]) -> Matrix {
        let v: Vec<Vec<u64>> = vec![vec![0, 0], vec![0, 0]];
        let m = Matrix { mat: v, shape };
        m
    }
}

pub mod generator {
    use rand::Rng;

    use super::Matrix;

    pub fn generate_matrix(shape: [usize; 2]) -> Matrix {
        let mut rng = rand::thread_rng();
        let mut m = Matrix::new(shape);

        for i in 0..shape[0] {
            for j in 0..shape[1] {
                m.mat[i][j] = rng.gen();
                m.mat[i][j] %= 5;
            }
        }
        m
    }
}
